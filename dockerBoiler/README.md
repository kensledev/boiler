# Docker 3 TEIR APP WITH PROXY


## APP | BACKEND/API | DB | PROXY

This is a boiler for a docker 3 tier app that uses nginx as a proxy. Each your front and backend should be Dockerized and both running on port 80. The compose file will remap the backend to port 4825(Soon /Admin).

- Frontend code && Dockerfile => ./app
- Backend/Api code && Dockerfile => ./back
- Uses MongoDb, Add the code at the bottom to the API to get connection Working


The APP and BACKEND/API run through the proxy.
The API then speaks to the DB

- APP runs on port 80     => Mapped to /
- API runs on port 4825  => Needs mapping to Admin
- DB(mongo) runs on port 27017  => Connected to API



## Mongo Node Connection

`
var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');
`


// Connection URL

`
var url = 'mongodb://localhost:27017/testdb';
`

// Use connect method to connect to the server

`
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected successfully to server");
  db.close();
});
`


# Command
`
docker-compose up --build
`
